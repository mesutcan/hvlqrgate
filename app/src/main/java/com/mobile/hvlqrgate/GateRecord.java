package com.mobile.hvlqrgate;

import java.io.Serializable;

public class GateRecord implements Serializable {
    private String qrId;
    private String gateType;
    private String entryType;
    private String personId;

    public GateRecord(String qrId, String gateType, String entryType, String personId) {
        this.qrId = qrId;
        this.gateType = gateType;
        this.entryType = entryType;
        this.personId = personId;
    }

    public String getQrId() {
        return qrId;
    }

    public void setQrId(String qrId) {
        this.qrId = qrId;
    }

    public String getGateType() {
        return gateType;
    }

    public void setGateType(String gateType) {
        this.gateType = gateType;
    }

    public String getEntryType() {
        return entryType;
    }

    public void setEntryType(String entryType) {
        this.entryType = entryType;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }
}
