package com.mobile.hvlqrgate;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface GateService {

    @POST("/app/saveGateRecord")
    Call<Object> saveGateRecord(@Body GateRecord gateRecord);

    @GET("/queue/add")
    Call<Integer> addQueue(@Query("empId") int empId);
}