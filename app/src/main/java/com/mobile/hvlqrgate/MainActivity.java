package com.mobile.hvlqrgate;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.zxing.Result;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private CodeScanner mCodeScanner;
    private String gateType,actionType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RadioGroup gateRadioGroup = findViewById(R.id.gate_type);
        gateRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.main_gate) {
                    gateType = "MAIN";
                } else if (checkedId == R.id.dining_hall) {
                    gateType = "DINING_HALL";
                } else if (checkedId == R.id.car_park) {
                    gateType = "CAR_PARK";
                } else if (checkedId == R.id.dining_queue) {
                    gateType = "DINING_QUEUE";
                }
            }
        });

        RadioGroup actionRadioGroup = findViewById(R.id.enter_exit);
        actionRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.enter) {
                    actionType = "ENTER";
                } else if (checkedId == R.id.exit) {
                    actionType = "EXIT";
                }
            }
        });

        CodeScannerView scannerView = findViewById(R.id.scanner_view);
        mCodeScanner = new CodeScanner(this, scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String[] seperated = result.getText().split(",");
                        GateService gateService = RetrofitClient.getRetrofitInstance().create(GateService.class);
                        if("DINING_QUEUE".equals(gateType)){
                            gateService.addQueue(Integer.parseInt(seperated[1])).enqueue(new Callback<Integer>() {
                                @Override
                                public void onResponse(@NonNull Call<Integer> call, @NonNull Response<Integer> response) {
                                    Toast.makeText(getApplicationContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onFailure(@NonNull Call<Integer> call, @NonNull Throwable t) {
                                    Toast.makeText(getApplicationContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        else
                        {
                            gateService.saveGateRecord(new GateRecord(seperated[0], gateType, actionType, seperated[1])).enqueue(new Callback<Object>() {
                                @Override
                                public void onResponse(@NonNull Call<Object> call, @NonNull Response<Object> response) {
                                    Toast.makeText(getApplicationContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onFailure(@NonNull Call<Object> call, @NonNull Throwable t) {
                                    Toast.makeText(getApplicationContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                });
            }
        });
        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCodeScanner.startPreview();
    }

    @Override
    protected void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }
}
